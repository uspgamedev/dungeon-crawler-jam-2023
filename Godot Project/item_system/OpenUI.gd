extends Node

@export var item_needed: Item.ItemType

signal correct_item
signal wrong_item

func _on_interactable_item_clicked():
	UI.getUI().increase_item_panel_size()
	UI.getUI().use_item.connect(check_item_needed, CONNECT_ONE_SHOT)

func check_item_needed(item: Item.ItemType):
	if (item_needed == item):
		correct_item.emit()
	else:
		wrong_item.emit()
		
