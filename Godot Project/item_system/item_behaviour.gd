extends Node

signal item_taken(type: Item.ItemType, texture: Texture)

@export var type: Item.ItemType
@export var mesh3D: MeshInstance3D
@export var item_texture: Texture

func _ready():
	self.add_to_group("items")

#func _on_interaction_item_clicked():
	#make the icon appear on the UI
	#emit_signal("item_taken", type, item_texture)
	#mesh3D.queue_free()

func _on_interactable_item_clicked():
	#make the icon appear on the UI
	emit_signal("item_taken", type, item_texture)
	self.queue_free()
