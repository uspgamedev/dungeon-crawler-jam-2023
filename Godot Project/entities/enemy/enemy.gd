extends CharacterBody3D
class_name Enemy

signal chase
@export var patrol_markers: Array[Marker3D] = []


func appear() -> void:
	$Visuals.visible = true


func disappear() -> void:
	$Visuals.visible = false


func _on_music_box_playing():
	$StateMachine.transition_to("Patrol")
