extends State


func enter(_msg := {}) -> void:
	var player = _msg["player"] as Player
	await player.block()
	# TODO: Wait for the enemy to finish their movement before rotating the player
	var enemy_direction: Vector3 = player.global_position.direction_to((owner as Enemy).global_position)
	await player.movement.rotate_to(enemy_direction)
	player.die()
