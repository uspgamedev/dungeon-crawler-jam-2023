extends State

const MIN_DIST := 16
@export var animation_duration := 3
@export var movement: MovementComponent
@export var small_area: Area3D
@onready var level_grid := get_tree().get_nodes_in_group("LevelGrid")[0] as LevelGrid
var current_path : = []


func physics_update(_delta: float) -> void:
	if current_path.size() > 0:
		var next_position: Vector3 = current_path.pop_front()
		var direction := (owner as Enemy).global_position.direction_to(next_position)
		state_machine.set_physics_process(false)
		await movement.rotate_to(direction, animation_duration * 0.2)
		await movement.move(direction, animation_duration * 0.8)
		state_machine.set_physics_process(true)
	else:
		var target := get_random_patrol_target()
		current_path = level_grid.get_astar_path((owner as Enemy).global_position, target.global_position)


func get_random_patrol_target() -> Marker3D:
	var random_marker: Marker3D = (owner as Enemy).patrol_markers.pick_random()
	while (owner as Enemy).global_position.distance_squared_to(random_marker.global_position) < MIN_DIST:
		random_marker = (owner as Enemy).patrol_markers.pick_random()
	return random_marker


func enter(_msg = {}) -> void:
	(owner as Enemy).disappear()
	small_area.body_entered.connect(_on_small_area_body_entered)
	small_area.set_monitoring(true)
	
	var random_marker := get_random_patrol_target()
	(owner as Enemy).global_position = random_marker.global_position
	(owner as Enemy).rotation_degrees = Vector3.ZERO


func exit() -> void:
	small_area.body_entered.disconnect(_on_small_area_body_entered)
	small_area.set_monitoring(false)


func _on_small_area_body_entered(body) -> void:
	state_machine.transition_to("Chase", { "player": body as Player })
