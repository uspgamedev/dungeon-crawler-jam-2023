extends State

@export var tiles_per_turn := 2
@export var await_actions := 3
@export var animation_duration := 1
@export var movement: MovementComponent
@export var small_area: Area3D
@onready var level_grid := get_tree().get_nodes_in_group("LevelGrid")[0] as LevelGrid
var player: Player


func chase(_arg) -> void:
	for i in tiles_per_turn:
		var path := level_grid.get_astar_path((owner as Enemy).global_position, player.global_position)
		if path.size() > 1:
			var direction := (owner as Enemy).global_position.direction_to(path[1])
			await movement.rotate_to(direction, animation_duration * 0.2)
			await movement.move(direction, animation_duration * 0.8)


func enter(_msg := {}) -> void:
	(owner as Enemy).chase.emit()
	(owner as Enemy).appear()
	
	player = _msg["player"] as Player
	await player.block()
	var player_direction := (owner as Enemy).global_position.direction_to(player.global_position)
	await movement.rotate_to(player_direction, animation_duration * 0.2)
	var enemy_direction := player.global_position.direction_to((owner as Enemy).global_position)
	await player.movement.rotate_to(enemy_direction)
	player.unblock()
	
	for i in await_actions:
		await player.action_ended
	
	player.action_ended.connect(chase)
	small_area.body_entered.connect(_on_small_area_body_entered)
	small_area.set_monitoring(true)


func exit() -> void:
	player.action_ended.disconnect(chase)
	small_area.body_entered.disconnect(_on_small_area_body_entered)
	small_area.set_monitoring(false)


func _on_small_area_body_entered(body):
	state_machine.transition_to("Kill", { "player": body as Player })
