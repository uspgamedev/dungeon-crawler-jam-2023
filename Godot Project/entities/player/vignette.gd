extends ColorRect
class_name Vignette

@onready var shader := (get_material() as ShaderMaterial)

func set_intensity(value: float, animate = false):
	if animate:
		var tween := create_tween()
		if tween:
			tween.tween_property(shader, "shader_parameter/vignette_intensity", value, 1)
			await tween.finished
	else:
		shader.set_shader_parameter("vignette_intensity", value)
