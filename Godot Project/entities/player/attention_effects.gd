extends Node

const MULTIPLIER := 2.5
@export var vignette: Vignette


func start(ratio: float):
	vignette.set_intensity(ratio * MULTIPLIER, true)


func update(ratio: float):
	vignette.set_intensity(ratio * MULTIPLIER, false)


func stop():
	vignette.set_intensity(0, true)
