extends CharacterBody3D
class_name Player

signal action_ended(action: Actions)

enum Actions {
		MOVE,
		ROTATE
}
const MAX_ENEMY_DISTANCE := 100
@export var movement: MovementComponent
@export var attention_effects: Node
@export var vignette: Vignette
var enemy_nearby := false
var enemy: Enemy


func _physics_process(_delta) -> void:
	if Input.is_action_pressed("LEFT"):
		do_action(Actions.MOVE, { "direction": -transform.basis.x })
	elif Input.is_action_pressed("RIGHT"):
		do_action(Actions.MOVE, { "direction": transform.basis.x })
	elif Input.is_action_pressed("FORWARD"):
		do_action(Actions.MOVE, { "direction": -transform.basis.z })
	elif Input.is_action_pressed("BACK"):
		do_action(Actions.MOVE, { "direction": transform.basis.z })
	elif Input.is_action_pressed("ROTATE_LEFT"):
		do_action(Actions.ROTATE, { "degrees": 90 })
	elif Input.is_action_pressed("ROTATE_RIGHT"):
		do_action(Actions.ROTATE, { "degrees": -90 })


func _process(_delta) -> void:
	if enemy_nearby:
		attention_effects.update(calculate_enemy_proximity())


func do_action(action: Actions, params = {}) -> void:
	set_physics_process(false)
	match action:
		Actions.MOVE:
				await movement.move(params.get("direction"))
		Actions.ROTATE:
				await movement.rotate_by(params.get("degrees"))
	action_ended.emit(action)
	set_physics_process(true)



func _on_enemy_entered_large_detection_area(_enemy) -> void:
	if on_sight(_enemy):
		enter_attention_state(_enemy)


func _on_enemy_exited_large_detection_area(_enemy) -> void:
	leave_attention_state()


func _on_enemy_entered_small_detection_area(_enemy) -> void:
	enter_attention_state(_enemy)


func on_sight(node: Node3D) -> bool:
	var raycast := RayCast3D.new()
	add_child(raycast)
	raycast.set_target_position(node.position)
	raycast.force_raycast_update()
	var colliding := raycast.is_colliding()
	raycast.queue_free()
	return not colliding


func enter_attention_state(_enemy: Enemy) -> void:
	enemy = _enemy
	await attention_effects.start(calculate_enemy_proximity())
	enemy_nearby = true


func leave_attention_state() -> void:
	enemy_nearby = false
	attention_effects.stop()


func calculate_enemy_proximity() -> float:
	var squared_distance := position.distance_squared_to(enemy.position)
	return (MAX_ENEMY_DISTANCE - squared_distance) / MAX_ENEMY_DISTANCE


func block() -> void:
	if not is_physics_processing():
		await action_ended
	set_physics_process(false)


func unblock() -> void:
	set_physics_process(true)


func die() -> void:
	await vignette.set_intensity(50, true)
	get_tree().quit()
