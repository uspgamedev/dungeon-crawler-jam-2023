extends Node

signal item_used(button_index: int)

@export var itemType:Array[Item.ItemType]

func _ready():
	self.add_to_group("PlayerInventory")
	
	# connect to the UI
	var uis = get_tree().get_nodes_in_group("UI")
	for ui in uis:
		ui.connect("use_item", use_item)
		print(ui.name)
	
	# for items that can be taken
	var items = get_tree().get_nodes_in_group("items")
	for item in items:
		item.connect("item_taken", add_item)
		print(item.name)
	
	#for items with just interaction and that needs another item (like a door needs a key)
	#var items_just_interactable = get_tree().get_nodes_in_group("item_interactable")
	#for item in items_just_interactable:
	#	item.connect("clicked", add_item)
	#	print(item.name)

func add_item(type: Item.ItemType, _texture):
	if (itemType[0] == Item.ItemType.NULL):
		itemType[0] = type
	elif (itemType[1] == Item.ItemType.NULL):
		itemType[1] = type
	elif (itemType[2] == Item.ItemType.NULL):
		itemType[2] = type
	elif (itemType[3] == Item.ItemType.NULL):
		itemType[3] = type
	elif (itemType[4] == Item.ItemType.NULL):
		itemType[4] = type
	else:
		print("There is no space in inventory")

func remove_item(index: int):
	itemType[index] = 0; # 0 = NULL
	

func use_item(needed_type: Item.ItemType, button_index: int):
	if (itemType[button_index] == needed_type):
		itemType[button_index] = Item.ItemType.NULL
		emit_signal("item_used", button_index)
		print("item used")
	else:
		print("the item used is not the item needed by the object")

#func check_item_in_inventory(type: Item.ItemType):
#	for item in itemType:
#		pass
		#if (item.Item.ItemType[0] == type):
			#print("yes, the player has the item")
