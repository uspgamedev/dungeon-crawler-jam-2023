extends Node3D
class_name MovementComponent

@export var animation_duration := 1.0
@onready var moving_entity := owner as Node3D
@onready var raycast := RayCast3D.new()
@onready var level_grid := get_tree().get_nodes_in_group("LevelGrid")[0] as LevelGrid


func _ready():
	moving_entity.add_child.call_deferred(raycast)


func move(direction: Vector3, _animation_duration := animation_duration):
	var target_position := moving_entity.global_position + level_grid.cell_size * direction
	if can_move(target_position):
		var tween := create_tween()
		tween.tween_property(
			moving_entity, 
			"global_position", 
			target_position,
			_animation_duration).set_trans(Tween.TRANS_CIRC).set_ease(Tween.EASE_OUT)
		await tween.finished


func can_move(target_position: Vector3):
	var tile := level_grid.local_to_map(level_grid.to_local(target_position))
	if level_grid.get_cell_item(tile) == -1:
		return false
	raycast.set_target_position(raycast.to_local(target_position))
	raycast.force_raycast_update()
	if raycast.is_colliding():
		return false
	return true


func rotate_by(_rotation: float, _animation_duration := animation_duration):
	var tween := create_tween()
	tween.tween_property(
		moving_entity, 
		"global_rotation_degrees:y", 
		_rotation,
		_animation_duration).as_relative().set_trans(Tween.TRANS_CIRC).set_ease(Tween.EASE_OUT)
	await tween.finished


func rotate_to(direction: Vector3, _animation_duration := animation_duration):
	var current_angle = moving_entity.global_rotation.y
	var target_angle = moving_entity.global_position.signed_angle_to(direction, Vector3.UP)
	var rotate_by = target_angle - current_angle
	rotate_by = snapped(rad_to_deg(rotate_by), 90)
	rotate_by = fmod(rotate_by + 180, 360) - 180
	await rotate_by(rotate_by, _animation_duration)
