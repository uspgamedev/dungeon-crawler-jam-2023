extends Control

@export var page_number := 0

@export_multiline var texts : Array[String] = []

func _ready():	
	if page_number < texts.size():
		%Label.text = texts[page_number]

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		queue_free()
