extends Control
class_name Pages

static func locate() -> Pages:
	return Engine.get_main_loop().get_first_node_in_group("PAGES")

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		%GridContainer.visible = false
	if event.is_action_pressed("SHOW_BOOK"):
		%GridContainer.visible = ! %GridContainer.visible

func _on_icon_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_LEFT:
		%GridContainer.visible = ! %GridContainer.visible

func new_page(page_number):
	var page = %GridContainer.get_children()[page_number]
	page.page_number = page_number
	page.enabled = true
