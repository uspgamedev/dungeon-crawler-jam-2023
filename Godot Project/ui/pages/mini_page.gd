extends TextureRect

@export var page_number := 0
@export var page_scene: PackedScene
@export var enabled = false : set = set_enabled

func _ready():
	set_enabled(enabled)

func set_enabled(enabled_):
	enabled = enabled_
	if enabled:
		self.modulate = Color.WHITE
	else:
		self.modulate = Color(0.3, 0.3, 0.3)

func _on_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_LEFT and enabled:
		var page = page_scene.instantiate()
		page.page_number = page_number
		Pages.locate().add_child(page)
