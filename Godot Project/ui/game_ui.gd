extends Control

class_name UI

signal button_pressed()
signal use_item(item: Item.ItemType)

@export var panel_buttons: Panel

@export var slot01_button: Button
@export var slot02_button: Button
@export var slot03_button: Button
@export var slot04_button: Button
@export var slot05_button: Button

# Called when the node enters the scene tree for the first time.
func _ready():
	self.add_to_group("UI")
	
	# connect with the inventory
	var inventories = get_tree().get_nodes_in_group("PlayerInventory")
	for inventory in inventories:
		inventory.connect("item_used", disable_single_button)
		inventory.connect("item_used", decrease_item_panel_size)
		print(inventory.name)
	
	# for items that can be taken
	var items = get_tree().get_nodes_in_group("items")
	for item in items:
		item.connect("item_taken", _on_item_behaviour_item_taken)
		print(item.name)


static func getUI() -> UI:
	return Engine.get_main_loop().get_first_node_in_group("UI")

func disable_single_button(button_index: int):
	if (button_index == 0):
		slot01_button.icon = null
	elif (button_index == 1):
		slot02_button.icon = null
	elif (button_index == 2):
		slot03_button.icon = null
	elif (button_index == 3):
		slot04_button.icon = null
	elif (button_index == 4):
		slot05_button.icon = null

# true for disabled and false for enabled
func disable_or_enable_buttons(value: bool):
	slot01_button.disabled = value
	slot02_button.disabled = value
	slot03_button.disabled = value
	slot04_button.disabled = value
	slot05_button.disabled = value

# add the item texture to the screen slot
func _on_item_behaviour_item_taken(_type, texture):
	if (slot01_button.icon == null):
		slot01_button.icon = texture
	elif (slot02_button.icon == null):
		slot02_button.icon = texture
	elif (slot03_button.icon == null):
		slot03_button.icon = texture
	elif (slot04_button.icon == null):
		slot04_button.icon = texture
	elif (slot05_button.icon == null):
		slot05_button.icon = texture
	else:
		print("There is no space in ui for new items")


# increase panel size when we need to select the item to be used
func increase_item_panel_size():
	var tween: Tween = create_tween()
	#var new_position = Vector2(0, -80)
	var new_size = Vector2(1.8, 1.8)
	#tween.parallel().tween_property(panel_buttons, "position", new_position,
		#0.5).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT)
	tween.parallel().tween_property(panel_buttons, "scale", new_size, 
		0.5).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT)

func decrease_item_panel_size():
	print("decreasing panel size")
	var tween: Tween = create_tween()
	#var new_position = Vector2(0, 0)
	var new_size = Vector2(1.0, 1.0)
	#tween.parallel().tween_property(panel_buttons, "position", new_position,
		#0.5).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT)
	tween.parallel().tween_property(panel_buttons, "scale", new_size, 
		0.5).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT)


func _on_object_need_item_clicked(type):
	increase_item_panel_size()


func _on_button_pressed():
	try_to_use_item(0)

func _on_button_2_pressed():
	try_to_use_item(1)

func _on_button_3_pressed():
	try_to_use_item(2)

func _on_button_4_pressed():
	try_to_use_item(3)

func _on_button_5_pressed():
	try_to_use_item(4)

func try_to_use_item(index: int):
	decrease_item_panel_size()
	var inventory = get_tree().get_first_node_in_group("PlayerInventory").itemType
	emit_signal("use_item", inventory[index])
	
