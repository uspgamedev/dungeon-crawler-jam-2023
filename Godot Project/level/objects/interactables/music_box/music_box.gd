extends Node3D

signal playing


func _ready() -> void:
	turn_on()


func turn_on() -> void:
	playing.emit()
	$Interactable.enabled = false


func turn_off() -> void:
	$Interactable.enabled = true
