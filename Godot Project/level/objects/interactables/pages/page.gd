extends Node3D

@export var page_number := 0


func _on_interactable_item_clicked():
	queue_free()
	Pages.locate().new_page(page_number)
