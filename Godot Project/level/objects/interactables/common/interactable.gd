extends Node
class_name Interactable

signal item_clicked

const INTERACTABLE_MASK := pow(2, 3-1)
const PLAYER_MASK := pow(2, 2-1)
@export var clickable_area3D: Area3D
@export var interactable_area3D: Area3D
@export var enabled := true
@export var one_shot := false
@onready var connections := {
	clickable_area3D: {
		"input_event": click_detection, 
		"mouse_entered": mouse_hover,
		"mouse_exited": mouse_out
	},
	interactable_area3D : {
		"body_entered": player_entered,
		"body_exited": player_exited
	}
}
var clickable := false


func _ready():
	clickable_area3D.set_collision_layer(INTERACTABLE_MASK)
	clickable_area3D.set_collision_mask(0)
	interactable_area3D.set_collision_layer(0)
	interactable_area3D.set_collision_mask(PLAYER_MASK)
	
	clickable_area3D.set_monitoring(false)

	for node in connections:
		var args: Dictionary = connections[node]
		for signal_name in args:
			var callable: Callable = args[signal_name]
			node.connect(signal_name, callable)


func click_detection(_camera, event, _position, _normal, _shape_idx):
	if clickable and event.is_action_pressed("CLICK"):
		mouse_out()
		item_clicked.emit()
		if one_shot:
			clickable = false
			enabled = false


func mouse_hover():
	if clickable:
		Input.set_default_cursor_shape(2)


func mouse_out():
	Input.set_default_cursor_shape(0)


func player_entered(_body: Node3D):
	if enabled:
		clickable = true
		clickable_area3D.set_monitoring(true)


func player_exited(_body: Node3D):
	clickable = false
	clickable_area3D.set_monitoring(false)
