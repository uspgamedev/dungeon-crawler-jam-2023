extends Node3D

@export var interactable: Interactable
@export var animation_player: AnimationPlayer
@export var animation_name: String


func _ready():
	interactable.item_clicked.connect(_on_interactable_item_clicked)


func _on_interactable_item_clicked():
	interactable.clickable = false
	animation_player.play(animation_name)
	await animation_player.animation_finished
	if not interactable.one_shot:
		interactable.clickable = true
