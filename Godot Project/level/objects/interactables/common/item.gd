extends Node
class_name Item

signal item_taken(type: ItemType, texture: Texture)

enum ItemType {
	NULL,
	IRON_KEY,
	BLUE_KEY_,
	RED_KEY_,
	GOLDEN_KEY,
	EYE,
	BOOK
}

@export var type: ItemType
@export var mesh3D: MeshInstance3D
@export var item_texture: Texture


func _ready():
	add_to_group("items")


#func _on_interaction_item_clicked():
	#make the icon appear on the UI
	#emit_signal("item_taken", type, item_texture)
	#mesh3D.queue_free()


func _on_interactable_item_clicked():
	#make the icon appear on the UI
	emit_signal("item_taken", type, item_texture)
	owner.queue_free()
