extends Node3D

enum RotationAxis{
	X,
	Y,
	Z
}

@export var rotation_axis : RotationAxis
@export var correct_rotation: float

signal painting_rotated
var rotation_angle = deg_to_rad(90)
var is_rotating = false
var finishedPuzzle = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$Interactable.connect("item_clicked", rotate_painting)
	var parent = get_parent().get_parent()
	if parent.has_method("onRotationChanged"):
		connect("painting_rotated", parent.onRotationChanged)
	
func rotate_painting():
	
	if is_rotating:
		return
	
	if finishedPuzzle:
		print("Tudo parece estar no lugar.")
		return
	
	is_rotating = true
	
	var tween = create_tween()
	tween.set_ease(3)
	
	match rotation_axis:
		RotationAxis.X:
			tween.tween_property(self, "rotation:x", rotation.x + rotation_angle, 0.5)
		RotationAxis.Y:
			tween.tween_property(self, "rotation:y", rotation.y + rotation_angle, 0.5)
		RotationAxis.Z:
			tween.tween_property(self, "rotation:z", rotation.z + rotation_angle, 0.5)
	
	tween.connect("finished", onTweenCompleted)
	
func onTweenCompleted():
	is_rotating = false
	self.rotation_degrees = normalize_rotation_vector(self.rotation_degrees)
	emit_signal("painting_rotated")

func normalize_rotation_vector(rotation: Vector3):
	return Vector3(
		fmod(rotation.x, 360.0),
		fmod(rotation.y, 360.0),
		fmod(rotation.z, 360.0)
	)

func getRotationAxis():
	return rotation_axis
	
func getCorrectRotation() -> int:
	return round(correct_rotation)
