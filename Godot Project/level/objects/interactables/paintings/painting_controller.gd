extends Node

func onRotationChanged():
	if checkOrder():
		var animation_player = get_parent().get_node("Showcase/ShowcaseGlass/OpenGlass")
		if animation_player and animation_player is AnimationPlayer:
			animation_player.connect("animation_finished", on_animation_finished)
			animation_player.play("opening")
		for child in get_children():
			for gchild in child.get_children():
				if gchild.has_method("getRotationAxis"):
					gchild.finishedPuzzle = true
		
func checkOrder():
	var correct = true
	
	for child in get_children():
		for gchild in child.get_children():
			if gchild is Node3D and gchild.has_method("getRotationAxis"):
				var rotationAxis = gchild.getRotationAxis()
				var correctRotation = gchild.getCorrectRotation()
				var actualRotation
				
				match rotationAxis:
					0: #RotationAxis.X
						actualRotation = round(gchild.rotation_degrees.x)
					1: #RotationAxis.Y
						actualRotation = round(gchild.rotation_degrees.y)
					2: #RotationAxis.Z
						actualRotation = round(gchild.rotation_degrees.z)	
				if(actualRotation!=correctRotation):
					correct = false
					
	return correct

func on_animation_finished():
	print("Fim")
