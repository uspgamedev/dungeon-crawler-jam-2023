extends Node3D


func turn_off() -> void:
	for light in get_children():
		light.get_node("AnimationPlayer").play("turn_off")


func turn_on() -> void:
	for light in get_children():
		light.get_node("AnimationPlayer").play_backwards("turn_off")
