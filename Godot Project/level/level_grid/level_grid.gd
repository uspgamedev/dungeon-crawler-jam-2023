extends GridMap
class_name LevelGrid

var astar := AStar3D.new()
var raycast: RayCast3D


func _ready() -> void:
	calculate_nav_grid()


func calculate_nav_grid():
	raycast = RayCast3D.new()
	add_child(raycast)
	var cells = get_used_cells()
	add_points_to_astar(cells)
	connect_neighbors(cells)
	raycast.queue_free()


func add_points_to_astar(cells) -> void:
	for cell in cells:
		astar.add_point(calculate_cell_index(cell), to_global(map_to_local(cell)))


func connect_neighbors(cells) -> void:
	for cell in cells:
		var index := calculate_cell_index(cell)
		var neighbors := get_neighbors(cell)
		for neighbor in neighbors:
			var neighbor_index := calculate_cell_index(neighbor)
			if astar.has_point(neighbor_index) \
			and not colliding(map_to_local(cell), map_to_local(neighbor)):
				astar.connect_points(index, neighbor_index)


func get_neighbors(cell: Vector3i) -> Array[Vector3i]:
	return [
		cell + Vector3i.RIGHT,
		cell + Vector3i.LEFT,
		cell + Vector3i.FORWARD,
		cell + Vector3i.BACK,
		cell + Vector3i.DOWN,
		cell + Vector3i.UP,
	]


func calculate_cell_index(cell: Vector3) -> int:
	return hash(cell)


func colliding(start: Vector3, end: Vector3) -> bool:
	raycast.set_position(start)
	raycast.set_target_position(end - start)
	raycast.force_raycast_update()
	return raycast.is_colliding()


func get_astar_path(from_position: Vector3, to_position: Vector3) -> PackedVector3Array:
	from_position = to_local(from_position)
	to_position = to_local(to_position)
	var start := calculate_cell_index(local_to_map(from_position))
	var end := calculate_cell_index(local_to_map(to_position))
	return astar.get_point_path(start, end)
